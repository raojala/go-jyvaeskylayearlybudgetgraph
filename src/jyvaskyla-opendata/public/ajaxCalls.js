// ajax call to get json data
function getJSON (url, callback)
{
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';

    xhr.onload = function() 
    {
        var status = xhr.status;

        if (status === 200) {
            callback(null, xhr.response);
        } 
        else {
            callback(status, xhr.response);
        }
    };

    xhr.send();
};

function yearlyTotalsCallback(err, data)
{
    
    data.reverse()
    let chartLabels = []
    let chartValues = []
    for (i = 0; i < data.length; i++) {
        
        chartLabels.push(data[i].Year +" | "+(data[i].Totals*0.01).toLocaleString()+" €")
        chartValues.push(data[i].Totals*0.01)  // convert from cents to euros
    }

    let values = {
        labels: chartLabels,
        series: [chartValues]
    };

    let options = {
        height: chartLabels.length*30+300,
        seriesBarDistance: 10,
        horizontalBars: true,
        axisY: {
            offset: 250
        },
        axisX: {
            offset: 300
        }
    };

    new Chartist.Bar('.ct-chart', values, options);
}

function suppliersTotalsCallback(err, data)
{
   
    let chartLabels = []
    let chartValues = []
    for (i = 0; i < data.length; i++) {
        
        chartLabels.push(data[i].ToimittajaNimi +" | "+(data[i].Paid*0.01).toLocaleString()+" €")
        chartValues.push(data[i].Paid*0.01)

    }

    let values = {
        labels: chartLabels,
        series: [chartValues]
    };

    let options = {
        height: chartLabels.length*30+300,
        seriesBarDistance: 10,
        horizontalBars: true,
        axisY: {
            offset: 500
        },
        axisX: {
            offset: 300
        }
    };

    new Chartist.Bar('.ct-chart', values, options);
}

function getCollectionsCallback(err, data) {

    data.sort()

    let selectElement = document.getElementById("collection")

    for (i = 0; i < data.length; i++) {
        let option = document.createElement('option');
        option.appendChild( document.createTextNode(data[i]));
        option.value = data[i]; 
        selectElement.appendChild(option); 
    }
}

function getGroupsCallback(err, data) {

    data.sort()

    let selectElement = document.getElementById("groups")

    for (i = 0; i < data.length; i++) {
        let option = document.createElement('option');
        option.appendChild( document.createTextNode(data[i]));
        option.value = data[i]; 
        selectElement.appendChild(option); 
    }
}

function yearlyTotals() {
    getJSON('/yearly-totals', yearlyTotalsCallback);

    changeChartTitle("Yearly totals")
}

function supplierTotals(collection) {
    getJSON('/supplier-totals?collection=' + collection, suppliersTotalsCallback);
}

function getCollections() {
    getJSON('/collections', getCollectionsCallback);
}

function getGroups(collection) {
    getJSON('/groups?collection=' + collection, getGroupsCallback);
}

function collectionChanged() {

    let selectElement = document.getElementById("collection")
    supplierTotals(selectElement.value)

    changeChartTitle(selectElement.value)
}

function groupChanged() {

    let selectElement = document.getElementById("groups")
    supplierTotals(selectElement.value)

    changeChartTitle(selectElement.value)
}

function changeChartTitle(title) {
    let chartTitle = document.getElementById("chartTitle")
    chartTitle.innerHTML = "";
    var content = document.createTextNode(title);
    chartTitle.appendChild(content);
}

function start() {
    yearlyTotals();
    getCollections();
    getGroups();
}

window.onload = start;
package main

import (
	"io/ioutil"
	"kulutus-seuranta/src/third-party/x2j"
	"os"
	"path/filepath"
	"strings"

	"github.com/tealeg/xlsx"
)

func exelToJSON() error {

	workDirectory, err := os.Getwd()
	if err != nil {
		return err
	}

	files, err := getExcelFilePaths()
	if err != nil {
		return err
	}

	existingFiles, err := getJSONFilePaths()
	if err != nil {
		return err
	}

	for i := 0; i < len(files); i++ {

		exelFilename := filepath.Base(files[i])
		targetJSONName := strings.ReplaceAll(exelFilename[:len(exelFilename)-len(filepath.Ext(exelFilename))], ".", "-") + ".json"

		IsLoaded := false
		for i := 0; i < len(existingFiles); i++ {
			existingFile := filepath.Base(existingFiles[i])
			if targetJSONName == existingFile {
				IsLoaded = true
			}
		}

		if IsLoaded == false {

			xFile, err := xlsx.OpenFile(files[i])
			if err != nil {
				return err
			}

			x2j := x2j.New()
			res, err := x2j.ToUpper().Convert(xFile)
			if err != nil {
				return err
			}

			if _, err := os.Stat(workDirectory + "/json/"); os.IsNotExist(err) {
				os.Mkdir(workDirectory+"/json/", os.ModeDir)
			}

			err = ioutil.WriteFile(workDirectory+"/json/"+targetJSONName, []byte(string(res)), 7777)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func getExcelFilePaths() ([]string, error) {
	var files []string

	workDirectory, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	err = filepath.Walk(workDirectory+"/exels",
		func(path string, info os.FileInfo, err error) error {
			if filepath.Ext(path) == ".xlsx" {
				files = append(files, path)
			}
			return nil
		},
	)

	return files, nil
}

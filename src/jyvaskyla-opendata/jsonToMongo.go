package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strconv"

	"go.mongodb.org/mongo-driver/mongo"
)

// KuluKokoelmaFile struct wraps the set of expense rows from json as array
type KuluKokoelmaFile struct {
	Rivit []KuluRiviFile `json:"Set"`
}

// KuluRiviFile is a struct used to parse in JSON file that has been
// parsed from exel
type KuluRiviFile struct {
	// tili, tilinro, tilit on sama
	// netto ja nettosumma sama
	Vastuualue      string `json:"Vastuualue"`
	ToimittajaNro   string `json:"Toimittaja nro"`
	ToimittajanNimi string `json:"Toimittajan nimi"`
	Tiliryhmä       string `json:"Tiliryhmä"`
	Tili            string `json:"Tili"`
	Tilit           string `json:"Tilit"`
	TiliNro         string `json:"Tili nro"`
	TilinNimi       string `json:"Tilin nimi"`
	Netto           string `json:"Netto"`
	Nettosumma      string `json:"Nettosumma"`
}

// KuluRiviMongo is a struct used to insert single expendature row to mongodb
type KuluRiviMongo struct {
	Vastuualue      string `bson:"Vastuualue"`
	ToimittajaNro   string `bson:"Toimittajanro"`
	ToimittajanNimi string `bson:"ToimittajanNimi"`
	Tiliryhmä       string `bson:"Tiliryhmä"`
	Tili            int    `bson:"Tili"`
	Tilit           int    `bson:"Tilit"`
	TiliNro         int    `bson:"TiliNro"`
	TilinNimi       string `bson:"TilinNimi"`
	Netto           int    `bson:"NettoCents"`
	Nettosumma      int    `bson:"NettosummaCents"`
}

func getJSONFilePaths() ([]string, error) {
	var files []string

	workDirectory, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	err = filepath.Walk(workDirectory+"/json",
		func(path string, info os.FileInfo, err error) error {
			files = append(files, path)
			return nil
		},
	)

	// return slice from index 1 forward because 0th index is the folder itself
	return files[1:], nil
}

// JSONToMongo looks files from workdir/json/ and inserts them to mongodb
func JSONToMongo(dbClient *MongoConnection, dbName string) error {

	// ISSUE: O^2 function. slow.
	// takes a long time to insert all rows to mongodb.
	existingCollections, err := dbClient.GetCollections(dbName)
	if err != nil {
		return err
	}

	jsons, err := getJSONFilePaths()
	if err != nil {
		return err
	}

	for i := 0; i < len(jsons); i++ {

		collectionName := filepath.Base(jsons[i])[:len(filepath.Base(jsons[i]))-len(filepath.Ext(jsons[i]))]

		// get year from exel name
		regExp, err := regexp.Compile("\\d{4}")
		if err != nil {
			fmt.Println(err)
		}
		collectionName = "ostot_" + regExp.FindString(collectionName)

		IsInDatabase := false
		for i := 0; i < len(existingCollections); i++ {
			if collectionName == existingCollections[i] {
				IsInDatabase = true
			}
		}

		if IsInDatabase == false {

			collection := dbClient.Client.Database(dbName).Collection(collectionName)

			jsonStruct, err := parseKuluKokoelmaFile(jsons[i])
			if err != nil {
				return err
			}

			// inserting one row at a time, because the JSON files are way over 16MB
			// and can't be inserted as is.
			for i := 0; i < len(jsonStruct.Rivit); i++ {

				err := insertRow(collection, &jsonStruct.Rivit[i])
				if err != nil {
					return err
				}
			}

			err = dbClient.CreateIndex(dbName, collectionName, "ToimittajanNimi")
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func insertRow(collection *mongo.Collection, jsonFromFile *KuluRiviFile) error {

	bson, err := convertKuluriviValues(jsonFromFile)
	if err != nil {
		return err
	}

	_, err = collection.InsertOne(ctx, bson)
	if err != nil {
		return err
	}

	return nil
}

func parseKuluKokoelmaFile(path string) (*KuluKokoelmaFile, error) {

	fileData, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var jsonStruct KuluKokoelmaFile
	err = json.Unmarshal(fileData, &jsonStruct)
	if err != nil {
		return nil, err
	}
	return &jsonStruct, nil
}

func convertKuluriviValues(input *KuluRiviFile) (*KuluRiviMongo, error) {

	var err error

	mongoFile := KuluRiviMongo{}
	mongoFile.Vastuualue = input.Vastuualue
	mongoFile.ToimittajaNro = input.ToimittajaNro
	mongoFile.ToimittajanNimi = input.ToimittajanNimi
	mongoFile.Tiliryhmä = input.Tiliryhmä

	if len(input.Tili) > 0 {
		mongoFile.Tili, err = strconv.Atoi(input.Tili)
		if err != nil {
			return nil, err
		}
	}

	if len(input.Tilit) > 0 {
		mongoFile.Tilit, err = strconv.Atoi(input.Tilit)
		if err != nil {
			return nil, err
		}
	}

	if len(input.TiliNro) > 0 {
		mongoFile.TiliNro, err = strconv.Atoi(input.TiliNro)
		if err != nil {
			return nil, err
		}
	}

	mongoFile.TilinNimi = input.TilinNimi

	if len(input.Netto) > 0 {
		floatNetto, err := strconv.ParseFloat(input.Netto, 64)
		if err != nil {
			return nil, err
		}
		mongoFile.Netto = int(floatNetto * 100)
	}

	if len(input.Nettosumma) > 0 {
		floatNetto, err := strconv.ParseFloat(input.Nettosumma, 64)
		if err != nil {
			return nil, err
		}
		mongoFile.Nettosumma = int(floatNetto * 100)
	}

	return &mongoFile, nil
}

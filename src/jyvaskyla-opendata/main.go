package main

import (
	"context"
	"fmt"
	"kulutus-seuranta/src/webtemplates"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

var privateKey string = "certs/key.pem"
var cert string = "certs/cert.pem"

var templates *webtemplates.Templates

var ctx context.Context
var cancel context.CancelFunc

var wg = sync.WaitGroup{}
var done chan byte = make(chan byte, 1)

var dbClient *MongoConnection
var dbName string = "Test"

func main() {

	var err error

	// gracefull exit
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	ctx = context.Background()
	ctx, cancel = context.WithCancel(ctx)
	defer cancel()

	err = getData()
	if err != nil {
		fmt.Println(err)
	}

	err = uncompressZips()
	if err != nil {
		log.Fatal(err)
	}

	err = exelToJSON()
	if err != nil {
		log.Fatal(err)
	}

	dbClient, err = newMongoConnection()
	if err != nil {
		log.Fatal(err)
	}
	defer dbClient.Client.Disconnect(ctx)

	err = JSONToMongo(dbClient, dbName)
	if err != nil {
		log.Fatal(err)
	}

	err = webProgram()
	if err != nil {
		log.Fatal(err)
	}

	// wait for ctrl+c
	<-sigs
	close(done)
	wg.Wait()
}

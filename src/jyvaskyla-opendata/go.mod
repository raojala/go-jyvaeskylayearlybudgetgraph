module kulutus-seuranta

go 1.15

require (
	github.com/kyokomi/x2j v0.0.0-20151229091347-4bae698a9737
	github.com/tealeg/xlsx v1.0.5
	go.mongodb.org/mongo-driver v1.4.2
)

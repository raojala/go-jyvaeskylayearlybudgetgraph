package main

import (
	"encoding/json"
	"fmt"
	"kulutus-seuranta/src/webprogram"
	"kulutus-seuranta/src/webtemplates"
	"net/http"
	"sort"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func webProgram() error {

	webMVC := webprogram.NewWebProgram()
	err := webMVC.LoadPEMFiles(cert, privateKey)
	if err != nil {
		return err
	}
	webMVC.CreateNewTLSServer()

	go func() {
		wg.Add(1)
		defer func() {
			fmt.Println("server.StartTLS()")
			wg.Done()
		}()

		if err := webMVC.StartTLS(); err != nil && err != http.ErrServerClosed {
			fmt.Println(err.Error())
		}
	}()

	go func() {
		wg.Add(1)
		defer func() {
			fmt.Println("server.StartRedirect()")
			wg.Done()
		}()

		if err := webMVC.StartRedirect(); err != nil && err != http.ErrServerClosed {
			fmt.Println(err.Error())
		}
	}()

	go func() {
		wg.Add(1)
		defer func() {
			fmt.Println("webMVC.Close(ctx)")
			wg.Done()
		}()

		select {
		case <-done:
			err = webMVC.Close(ctx)
			if err != nil {
				fmt.Println(err)
			}
		}
	}()

	registerRoutes()
	webMVC.StartPublicFileServer("/public/")

	return nil
}

func registerRoutes() {
	templates = webtemplates.LoadPageTemplates("templates/*.gohtml")
	http.HandleFunc("/", index)
	http.HandleFunc("/yearly-totals", yearlyTotals)
	http.HandleFunc("/supplier-totals", supplierTotals)
	http.HandleFunc("/collections", getCollections)
	http.HandleFunc("/groups", groupTotals)

}

func index(rw http.ResponseWriter, request *http.Request) {

	if err := templates.Pages.ExecuteTemplate(rw, "index.gohtml", nil); err != nil {
		fmt.Println(err)
		http.Error(rw, "Internal server error", http.StatusInternalServerError)
	}
}

type sumRow struct {
	Year   string
	Totals int
}

func yearlyTotals(rw http.ResponseWriter, request *http.Request) {

	groupBSON := bson.D{
		primitive.E{
			Key: "$group", Value: bson.D{
				primitive.E{
					Key:   "_id",
					Value: "",
				},
				primitive.E{
					Key: "total1",
					Value: bson.D{
						primitive.E{
							Key:   "$sum",
							Value: "$NettosummaCents",
						},
					},
				},
				primitive.E{
					Key: "total2",
					Value: bson.D{
						primitive.E{
							Key:   "$sum",
							Value: "$NettoCents",
						},
					},
				},
			},
		},
	}

	projectBSON := bson.D{
		primitive.E{
			Key: "$project",
			Value: bson.D{
				primitive.E{
					Key:   "_id",
					Value: 0,
				},
			},
		},
	}

	collectionNames, err := dbClient.GetCollections(dbName)
	if err != nil {
		fmt.Println(err)
		http.Error(rw, "Internal server error", http.StatusInternalServerError)
		return
	}
	sort.Strings(collectionNames)

	keyValueMap := make([]sumRow, len(collectionNames))
	for i, collectionName := range collectionNames {

		collection := dbClient.Client.Database(dbName).Collection(collectionName)
		mongoCursor, err := collection.Aggregate(ctx, mongo.Pipeline{groupBSON, projectBSON})
		if err != nil {
			fmt.Println(err)
			http.Error(rw, "Internal server error", http.StatusInternalServerError)
			return
		}

		var totals []map[string]int
		if err = mongoCursor.All(ctx, &totals); err != nil {
			fmt.Println(err)
			http.Error(rw, "Internal server error", http.StatusInternalServerError)
			return
		}

		keyValueMap[i] = sumRow{Year: collectionName, Totals: totals[0]["total1"] + totals[0]["total2"]}
	}

	returnMarshal, err := json.Marshal(keyValueMap)
	if err != nil {
		fmt.Println(err)
		http.Error(rw, "Internal server error", http.StatusInternalServerError)
		return
	}

	rw.Write(returnMarshal)
}

func supplierTotals(rw http.ResponseWriter, request *http.Request) {

	collection := request.FormValue("collection")

	if len(collection) > 0 {

		mapThing, err := dbClient.GetExpenseTotalForToimittaja(dbName, collection)
		if err != nil {
			fmt.Println(err)
			http.Error(rw, "Internal server error", http.StatusInternalServerError)
			return
		}

		returnMarshal, err := json.Marshal(mapThing)
		if err != nil {
			fmt.Println(err)
			http.Error(rw, "Internal server error", http.StatusInternalServerError)
			return
		}

		rw.Write(returnMarshal)
	} else {
		rw.Write([]byte("{}"))
	}

}

func getCollections(rw http.ResponseWriter, request *http.Request) {

	mapThing, err := dbClient.GetCollections(dbName)
	if err != nil {
		fmt.Println(err)
		http.Error(rw, "Internal server error", http.StatusInternalServerError)
		return
	}

	returnMarshal, err := json.Marshal(mapThing)
	if err != nil {
		fmt.Println(err)
		http.Error(rw, "Internal server error", http.StatusInternalServerError)
		return
	}

	rw.Write(returnMarshal)
}

func getGroups(rw http.ResponseWriter, request *http.Request) {

	mapThing, err := dbClient.GetCollections(dbName)
	if err != nil {
		fmt.Println(err)
		http.Error(rw, "Internal server error", http.StatusInternalServerError)
		return
	}

	returnMarshal, err := json.Marshal(mapThing)
	if err != nil {
		fmt.Println(err)
		http.Error(rw, "Internal server error", http.StatusInternalServerError)
		return
	}

	rw.Write(returnMarshal)
}


func groupTotals(rw http.ResponseWriter, request *http.Request) {

	collection := request.FormValue("collection")

	if len(collection) > 0 {
		mapThing, err := dbClient.GetExpenseTotalForVastuualue(dbName, collection)
		if err != nil {
			fmt.Println(err)
			http.Error(rw, "Internal server error", http.StatusInternalServerError)
			return
		}

		returnMarshal, err := json.Marshal(mapThing)
		if err != nil {
			fmt.Println(err)
			http.Error(rw, "Internal server error", http.StatusInternalServerError)
			return
		}

		rw.Write(returnMarshal)
	} else {
		rw.Write([]byte("{}"))
	}
}

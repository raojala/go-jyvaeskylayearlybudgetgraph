package main

import (
	"archive/zip"
	"io"
	"os"
	"path/filepath"
	"strings"
)

func uncompressZips() error {

	files, err := getZipFilePaths()
	if err != nil {
		return err
	}

	existingFiles, err := getExcelFilePaths()
	if err != nil {
		return err
	}

	// iterate through zips
	for i := 0; i < len(files); i++ {

		// open zip archive
		zip, err := zip.OpenReader(files[i])
		if err != nil {
			return err
		}
		defer zip.Close()

		// iterate through files inside zip
		for _, b := range zip.File {

			if !strings.Contains(b.Name, "__MACOSX") {

				// check if target exel already exists
				IsLoaded := false
				for i := 0; i < len(existingFiles); i++ {
					filename := filepath.Base(existingFiles[i])
					if b.Name == filename {
						IsLoaded = true
					}
				}

				if IsLoaded == false {

					unzipFile(b)
				}
			}
		}
	}

	return nil
}

func getZipFilePaths() ([]string, error) {
	var files []string

	workDirectory, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	err = filepath.Walk(workDirectory+"/zips",
		func(path string, info os.FileInfo, err error) error {
			files = append(files, path)
			return nil
		},
	)

	return files[1:], nil
}

func unzipFile(archive *zip.File) error {

	workDirectory, err := os.Getwd()
	if err != nil {
		return err
	}

	// check flder
	if _, err := os.Stat(workDirectory + "/exels/"); os.IsNotExist(err) {
		os.Mkdir(workDirectory+"/exels/", os.ModeDir)
	}

	// Create the file
	out, err := os.Create(workDirectory + "/exels/" + archive.Name)
	if err != nil {
		return err
	}
	defer out.Close()

	body, err := archive.Open()
	if err != nil {
		return err
	}
	defer body.Close()

	// Write the body to file
	_, err = io.Copy(out, body)
	if err != nil {
		return err
	}

	return nil
}

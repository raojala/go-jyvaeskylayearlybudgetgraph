package main

import (
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoConnection ..
type MongoConnection struct {
	Client *mongo.Client
}

func newMongoConnection() (*MongoConnection, error) {

	mc := MongoConnection{}

	credential := options.Credential{
		Username: "mongoadmin",
		Password: "secret",
	}

	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://192.168.0.107:12345").SetAuth(credential))
	if err != nil {
		return nil, err
	}

	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}

	mc.Client = client

	return &mc, nil
}

// CreateIndex adds an index to collection for given value.
func (MC *MongoConnection) CreateIndex(dbName, collectionName, indexKey string) error {

	index := mongo.IndexModel{}
	index.Keys = bson.D{primitive.E{Key: indexKey, Value: 1}}

	_, err := MC.Client.Database(dbName).Collection(collectionName).Indexes().CreateOne(ctx, index)
	if err != nil {
		return err
	}

	return nil
}

// GetCollections returns a string array containing the names of collections
// inside database.
func (MC *MongoConnection) GetCollections(dbName string) ([]string, error) {

	collections, err := MC.Client.Database(dbName).ListCollectionNames(ctx, bson.D{})
	if err != nil {
		return nil, err
	}

	return collections, nil
}

// GetUniqueToimittajaNimet returns a string array containing unique names of suppliers
func (MC *MongoConnection) GetUniqueToimittajaNimet(dbName, collectionName string) ([]string, error) {

	collection := MC.Client.Database(dbName).Collection(collectionName)

	suppliers, err := collection.Distinct(ctx, "ToimittajanNimi", bson.D{})
	if err != nil {
		return nil, err
	}

	returnStringArray := make([]string, 0)
	for _, value := range suppliers {
		stringValue := fmt.Sprintf("%v", value)
		returnStringArray = append(returnStringArray, stringValue)
	}

	return returnStringArray, nil
}

// GetUniqueVastuualueet returns a string array containing unique groups
func (MC *MongoConnection) GetUniqueVastuualueet(dbName, collectionName string) ([]string, error) {

	collection := MC.Client.Database(dbName).Collection(collectionName)

	vastuualueet, err := collection.Distinct(ctx, "Vastuualue", bson.D{})
	if err != nil {
		return nil, err
	}

	returnStringArray := make([]string, 0)
	for _, value := range vastuualueet {
		stringValue := fmt.Sprintf("%v", value)
		returnStringArray = append(returnStringArray, stringValue)
	}

	return returnStringArray, nil
}

// ToimittajaTotalPaid holds the sum data for single supplier
type ToimittajaTotalPaid struct {
	ToimittajaNimi string `bson:"_id"`
	Paid           int    `bson:"totalPaid"`
}

// GetExpenseTotalForToimittaja returns a map of unique suppliers and total amount of paid to them
func (MC *MongoConnection) GetExpenseTotalForToimittaja(dbName, collectionName string) ([]ToimittajaTotalPaid, error) {

	groupBSON := bson.D{
		primitive.E{
			Key: "$group", Value: bson.D{
				primitive.E{
					Key:   "_id",
					Value: "",
				},
				primitive.E{
					Key: "total1",
					Value: bson.D{
						primitive.E{
							Key:   "$sum",
							Value: "$NettosummaCents",
						},
					},
				},
				primitive.E{
					Key: "total2",
					Value: bson.D{
						primitive.E{
							Key:   "$sum",
							Value: "$NettoCents",
						},
					},
				},
			},
		},
	}

	projectBSON := bson.D{
		primitive.E{
			Key: "$project",
			Value: bson.D{
				primitive.E{
					Key:   "_id",
					Value: 0,
				},
			},
		},
	}

	toimittajat, err := dbClient.GetUniqueToimittajaNimet(dbName, collectionName)
	if err != nil {
		log.Fatal(err)
	}

	returnArray := make([]ToimittajaTotalPaid, 0)

	fromCollection := dbClient.Client.Database(dbName).Collection(collectionName)
	for _, toimittaja := range toimittajat {

		matchBSON := bson.D{
			primitive.E{
				Key: "$match",
				Value: bson.D{
					primitive.E{
						Key:   "ToimittajanNimi",
						Value: toimittaja,
					},
				},
			},
		}

		mongoCursor, err := fromCollection.Aggregate(ctx, mongo.Pipeline{matchBSON, groupBSON, projectBSON})
		if err != nil {
			return nil, err
		}

		var totals []map[string]int
		if err = mongoCursor.All(ctx, &totals); err != nil {
			return nil, err
		}

		toimittajaKooste := ToimittajaTotalPaid{}
		toimittajaKooste.ToimittajaNimi = toimittaja
		toimittajaKooste.Paid = totals[0]["total1"] + totals[0]["total2"]
		returnArray = append(returnArray, toimittajaKooste)
	}

	return returnArray, nil
}

// VastuualueTotalPaid holds the sum data for single supplier
type VastuualueTotalPaid struct {
	VastuualueName string `bson:"_id"`
	Paid           int    `bson:"totalPaid"`
}

// GetExpenseTotalForVastuualue returns a map of unique groups and total amount of paid to them
func (MC *MongoConnection) GetExpenseTotalForVastuualue(dbName, collectionName string) ([]VastuualueTotalPaid, error) {

	groupBSON := bson.D{
		primitive.E{
			Key: "$group", Value: bson.D{
				primitive.E{
					Key:   "_id",
					Value: "",
				},
				primitive.E{
					Key: "total1",
					Value: bson.D{
						primitive.E{
							Key:   "$sum",
							Value: "$NettosummaCents",
						},
					},
				},
				primitive.E{
					Key: "total2",
					Value: bson.D{
						primitive.E{
							Key:   "$sum",
							Value: "$NettoCents",
						},
					},
				},
			},
		},
	}

	projectBSON := bson.D{
		primitive.E{
			Key: "$project",
			Value: bson.D{
				primitive.E{
					Key:   "_id",
					Value: 0,
				},
			},
		},
	}

	vastuualueet, err := dbClient.GetUniqueVastuualueet(dbName, collectionName)
	if err != nil {
		log.Fatal(err)
	}

	returnArray := make([]VastuualueTotalPaid, 0)

	fromCollection := dbClient.Client.Database(dbName).Collection(collectionName)
	for _, vastuualue := range vastuualueet {

		matchBSON := bson.D{
			primitive.E{
				Key: "$match",
				Value: bson.D{
					primitive.E{
						Key:   "Vastuualue",
						Value: vastuualue,
					},
				},
			},
		}

		mongoCursor, err := fromCollection.Aggregate(ctx, mongo.Pipeline{matchBSON, groupBSON, projectBSON})
		if err != nil {
			return nil, err
		}

		var totals []map[string]int
		if err = mongoCursor.All(ctx, &totals); err != nil {
			return nil, err
		}

		vastuualueKooste := VastuualueTotalPaid{}
		vastuualueKooste.VastuualueName = vastuualue
		vastuualueKooste.Paid = totals[0]["total1"] + totals[0]["total2"]

		returnArray = append(returnArray, vastuualueKooste)
	}

	return returnArray, nil
}

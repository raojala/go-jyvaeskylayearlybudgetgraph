package main

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"regexp"
)

func getData() error {

	path := "https://data.jyvaskyla.fi/data.php"

	// https://data.jyvaskyla.fi/tiedostot/ostot_2018.xlsx.zip
	htmlSource, err := getHTMLsource(path)
	if err != nil {
		return err
	}

	links, err := getLinks(htmlSource)
	if err != nil {
		return err
	}

	u, err := url.Parse(path)
	if err != nil {
		return err
	}

	existingFiles, err := getZipFilePaths()
	if err != nil {
		return err
	}

	for i := 0; i < len(links); i++ {

		filename := filepath.Base(links[i])

		IsLoaded := false
		for i := 0; i < len(existingFiles); i++ {
			zipName := filepath.Base(existingFiles[i])
			if filename == zipName {
				IsLoaded = true
			}
		}

		if IsLoaded == false {
			path := "https://" + u.Host + "/" + links[i]
			err = downloadFile(path)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func getHTMLsource(url string) (string, error) {

	response, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	html, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	return string(html), nil
}

func getLinks(source string) ([]string, error) {

	regExp, err := regexp.Compile("(?s)Jyväskylän kaupungin ostot.*?Päivitys")
	if err != nil {
		return nil, err
	}
	ostotSource := regExp.FindString(source)

	regExp2, err := regexp.Compile("(?s)tiedostot/ostot.*?zip")
	if err != nil {
		return nil, err
	}

	return regExp2.FindAllString(ostotSource, -1), nil
}

func downloadFile(link string) error {

	workDirectory, err := os.Getwd()
	if err != nil {
		return err
	}

	urlParts, err := url.Parse(link)
	if err != nil {
		return err
	}

	// Get the data
	response, err := http.Get(link)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	// check flder
	if _, err := os.Stat(workDirectory + "/zips/"); os.IsNotExist(err) {
		os.Mkdir(workDirectory+"/zips/", os.ModeDir)
	}

	// Create the file
	out, err := os.Create(workDirectory + "/zips/" + path.Base(urlParts.Path))
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, response.Body)
	return err
}

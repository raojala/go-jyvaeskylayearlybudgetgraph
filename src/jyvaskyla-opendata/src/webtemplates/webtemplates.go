package webtemplates

import (
	"fmt"
	"text/template"
	"time"
)

// Templates struct defines the holder type for wep page gohtml templates
type Templates struct {
	Pages       *template.Template
	FunctionMap template.FuncMap
}

// LoadPageTemplates creates and returns a handle for Templates type, that holds gohtml views and custom functions
func LoadPageTemplates(templateDir string) *Templates {

	webTemplates := Templates{}

	webTemplates.FunctionMap = template.FuncMap{}
	webTemplates.AddFunctionToFuncMap("formatAsDate", formatAsDate)

	webTemplates.Pages = template.Must(template.New("main").Funcs(webTemplates.FunctionMap).ParseGlob(templateDir))
	return &webTemplates
}

// AddFunctionToFuncMap adds a function to template.FuncMap, so that it can be used in gohtml files.
func (T *Templates) AddFunctionToFuncMap(funcName string, f interface{}) {
	T.FunctionMap[funcName] = f
}

func formatAsDate(t time.Time) string {
	year, month, day := t.Date()
	return fmt.Sprintf("%d.%d.%d", day, month, year)
}

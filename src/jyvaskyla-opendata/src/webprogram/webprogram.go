package webprogram

import (
	"context"
	"crypto/tls"
	"errors"
	"net/http"
	"os"
)

// WebProgram is a TLS HTTPS server abstraction that redirects 80 and 8080 to 443, it also handles starting and closing the server
type WebProgram struct {
	SecureServer     *http.Server
	InsecureServer   *http.Server
	Conf             *tls.Config
	PublicFolderPath string
}

// NewWebProgram default constructor for WebProgram
func NewWebProgram() *WebProgram {

	server := WebProgram{}
	return &server
}

// LoadPEMFiles loads the pem file used to encrypt internet traffic (using pem because letsencrypt returns PEM file). Returns error.
func (W *WebProgram) LoadPEMFiles(certPath, privateKeyPath string) error {

	cert, err := tls.LoadX509KeyPair(certPath, privateKeyPath)
	if err != nil {
		return err
	}

	W.Conf = &tls.Config{Certificates: []tls.Certificate{cert}}

	return nil
}

// CreateNewTLSServer Creates a new HTTPS server using loaded PEM file. Also initializes Insecure HTTP server for HTTPS redirect.
func (W *WebProgram) CreateNewTLSServer() {

	W.SecureServer = &http.Server{
		Addr:      ":https",
		TLSConfig: W.Conf,
	}

	// Listen HTTP ports and set redirects to HTTPS
	W.InsecureServer = &http.Server{
		Addr:    ":80",
		Handler: http.HandlerFunc(redirectHTTPToHTTPS),
	}
}

// StartTLS function starts HTTPS server. Returns a handle to the server for the user.
func (W *WebProgram) StartTLS() error {

	return W.SecureServer.ListenAndServeTLS("", "")
}

// StartRedirect function starts HTTP server. Returns a handle to the server for the user.
func (W *WebProgram) StartRedirect() error {

	return W.InsecureServer.ListenAndServe()
}

// Close function handles both GTTP and HTTPS server shutdowns with context. Returns error if any.
func (W *WebProgram) Close(ctx context.Context) error {

	var errMessage string = ""

	err1 := W.InsecureServer.Shutdown(ctx)
	if err1 != context.Canceled && err1 != nil {
		errMessage = "\n\nInsecureServer shutdown error\n" + err1.Error()
	}

	err2 := W.SecureServer.Shutdown(ctx)
	if err2 != context.Canceled && err2 != nil {
		errMessage = errMessage + "\n\nSecureServer shutdown error\n" + err2.Error()
	}

	if len(errMessage) > 0 {
		return errors.New(errMessage)
	}

	return nil
}

// StartPublicFileServer runs fileserver for css files and such
func (W *WebProgram) StartPublicFileServer(path string) {
	W.PublicFolderPath = path
	http.HandleFunc(path, W.fileServer)
}

func redirectHTTPToHTTPS(rw http.ResponseWriter, req *http.Request) {

	http.Redirect(rw, req, "https://"+req.Host+req.URL.String(), http.StatusMovedPermanently)
}

func (W *WebProgram) fileServer(rw http.ResponseWriter, request *http.Request) {

	url := request.URL.Path[1:]
	file, err := os.Stat(url)
	if err != nil {
		http.Redirect(rw, request, "/", http.StatusSeeOther)
	} else if file.Mode().IsDir() {
		http.Redirect(rw, request, "/", http.StatusSeeOther)
	}
	http.StripPrefix(W.PublicFolderPath, http.FileServer(http.Dir(W.PublicFolderPath[1:len(W.PublicFolderPath)-1]))).ServeHTTP(rw, request)
}

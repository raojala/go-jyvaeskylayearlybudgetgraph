package x2j

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/tealeg/xlsx"
)

// original: "github.com/kyokomi/x2j"

var defaultX2JSON = New()

// Exel2JSON ..
type Exel2JSON struct {
	toLower bool
	toUpper bool
}

// New ..
func New() Exel2JSON {
	return Exel2JSON{}
}

// ToUpper ..
func (x *Exel2JSON) ToUpper() *Exel2JSON {
	x.toUpper = true
	x.toLower = false
	return x
}

// ToLower ..
func (x *Exel2JSON) ToLower() *Exel2JSON {
	x.toUpper = false
	x.toLower = true
	return x
}

func (x *Exel2JSON) toCase(s string) string {
	if x.toLower {
		return strings.ToLower(s)
	} else if x.toUpper {
		return strings.ToUpper(s)
	}
	return s
}

func (x *Exel2JSON) sheet2Map(sheet *xlsx.Sheet) ([]map[string]string, error) {
	if len(sheet.Rows) < 1 {
		return nil, fmt.Errorf("sheet rows error")
	}

	titles := make([]string, len(sheet.Rows[0].Cells))
	for i, c := range sheet.Rows[0].Cells {
		// titles[i] = x.toCase(strings.c.Value)
		titles[i] = x.toCase(strings.TrimSpace(c.Value))
	}

	//	converts := make([]map[string]string, len(sheet.Rows[1:]))
	converts := make([]map[string]string, 0)
	for _, r := range sheet.Rows[1:] {

		if len(r.Cells[0].Value) > 0 {
			convertMap := map[string]string{}

			for j := 0; j < len(titles); j++ {
				if j >= len(r.Cells) {
					convertMap[titles[j]] = ""
				} else {
					convertMap[titles[j]] = r.Cells[j].Value
				}
			}
			// converts[i] = convertMap
			converts = append(converts, convertMap)
		}
	}

	return converts, nil
}

func (x *Exel2JSON) xlsx2Map(xFile *xlsx.File) map[string][]map[string]string {
	responseJSON := map[string][]map[string]string{}
	for _, s := range xFile.Sheets {
		c, err := x.sheet2Map(s)
		if err != nil {
			continue
		}
		// responseJSON[x.toCase(s.Name)] = c
		responseJSON["Set"] = c
	}
	return responseJSON
}

// Convert ..
func (x *Exel2JSON) Convert(xFile *xlsx.File) (json.RawMessage, error) {
	data, err := json.Marshal(x.xlsx2Map(xFile))
	if err != nil {
		return nil, err
	}

	return json.RawMessage(data), nil
}

// Convert ..
func Convert(xFile *xlsx.File) (json.RawMessage, error) {
	return defaultX2JSON.Convert(xFile)
}

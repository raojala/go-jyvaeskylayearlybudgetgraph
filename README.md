# Go-JyväskylaYearlyBudgetGraph

A Go Web server that displays Jyväskylä yearly budgets as graphs using chartist.js. Gets excels and converts then to JSON and finally stores them in MongoDB.

## How?

Data is from https://data.jyvaskyla.fi/data.php. Web server sends a http request for the website. which returns the html source. Regex is used to parse the proper links and http request is made for each of those links returning a byte stream of ZIP archive data.

Zips are uncompressed using archive/zip library.

Excel files are converted to JSON by using a "github.com/tealeg/xlsx" and slightly altered "github.com/kyokomi/x2j" to suite the projects needs.

converted JSON files are stored in MongoDB document database and indexed by "ToimittajaNimi" field, which is required to query the documents fast (8 minute yearly budget query reduced to 10 second ~ish).

After these are done, a web server starts listening on port 443 using self generated (or certbot) certificates and redirects all http requests to https, not allowing insecure connections.

## About

This project was halted unfinished. Only one the planned charts was finished (yearly budgets). Do not take this project to represent Go in any shape or form. Ugly globals were used as shortcuts and the project is in "Just get it working" phase.

### Screenshots

![](images/yearlyTotals.jpg)
Yearly totals graph.  

Drafts of supplier totals per year from 2012.  
![](images/supplierTotalPerYear.jpg)
![](images/supplierTotalPerYearDraft.jpg)












